import styled from "styled-components";

const SInput = styled.div`
    background-color:#16a3ff;
    padding: 5px;
    border-radius:5px;
    margin:5px;
    width: auto;
    color:#fff;
`
export default SInput