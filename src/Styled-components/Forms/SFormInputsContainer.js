import styled from "styled-components";

const SFormInputsContainer = styled.div`
    display:flex;
    flex-wrap:wrap;
    width: 100%;
    height: auto;
`
export default SFormInputsContainer