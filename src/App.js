import React, { useState } from 'react';
import Layout from './Styled-components/Layout/Layout';
import SearchInput from './Styled-components/Search/SearchInput';
import FormContainer from './Components/FormContainer/FormContainer';
import Axios from 'axios';
import cheerio from 'cheerio';

function App() {
  const [$forms, setForms] = useState([]);

  const handleKeyPress = (event) => {
    if (event.keyCode === 13) {
      Axios.get(event.target.value).then((html) => {
        const $ = cheerio.load(html);
        setForms($('form'));
      });
    }
  };

  return (
    <Layout>
      <SearchInput
        onKeyDown={(event) => handleKeyPress(event)}
        placeholder='Enter your form URL'></SearchInput>
      {}
      <h2>Forms Found in the Provided Link:</h2>
      <FormContainer forms={$forms}></FormContainer>
    </Layout>
  );
}

export default App;
