import React from 'react';
import SFormsContainer from '../../Styled-components/Forms/FormContainer';
import FormItem from '../FormItem/FormItem';

import { array } from 'prop-types';

const FormContainer = (props) => {
  const { forms } = props;

  return (
    <SFormsContainer>
      {forms.map((form) => (
        <FormItem form={form} />
      ))}
    </SFormsContainer>
  );
};

FormContainer.defaultProps = {
  forms: []
};

FormContainer.propTypes = {
  forms: array
};

export default FormContainer;
