import styled from "styled-components";

 const SFormsContainer= styled.div`
  display: grid;
  width:100%;
  grid-auto-rows:minmax(120px, auto);
  grid-gap: 20px;
`
export default SFormsContainer