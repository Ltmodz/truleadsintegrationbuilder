import styled from "styled-components";

const SCard = styled.div`
padding:10px 20px;
border-radius:5px;
background:#fff;
cursor: pointer;
transition:0.3s ease-in-out;
box-shadow: 0px 0px 20px 0px rgba(0,0,0,0.2);
&:hover{
    transform:scale(1.02);
}
`

export default SCard;