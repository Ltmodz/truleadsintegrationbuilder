import styled from 'styled-components';

const Layout = styled.div`
 width: 100%;
 height: 100%;
 overflow-y:auto;
 display: flex;
 flex-direction:column;
 background-color:#bedbff;
 padding:40px;

`
export default Layout;