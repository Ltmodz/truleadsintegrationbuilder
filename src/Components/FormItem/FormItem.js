import React from 'react'
import SCard from '../../Styled-components/Utils/Card'
import SFormItem from '../../Styled-components/Forms/FormItem'
import SFormInputsContainer from '../../Styled-components/Forms/SFormInputsContainer'
import SInput from '../../Styled-components/Inputs/SInput'

export default function FormItem() {
    return (
        <SCard>
            <SFormItem>
                <h4>Form Info:</h4>
                <ul>
                    <li>form id: 1</li>
                    <li>form type: post</li>
                    <li>form class: post</li>
                </ul>
                <h4>Inputs of This Form:</h4>
                <SFormInputsContainer>
                    <SInput>Phone number</SInput>
                </SFormInputsContainer>
            </SFormItem>
        </SCard>
    )
}
