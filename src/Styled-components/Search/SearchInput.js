import styled from 'styled-components';


const SearchInput = styled.input`
    padding: 10px;
    background-color:#fff;
    border:none;
    outline:none;
    border-radius:5px;
    font-size:18px;
    transition:0.3s;
    box-shadow: 0px 0px 20px 0px rgba(0,0,0,0.2);
    margin-bottom:30px;
`
export default SearchInput;